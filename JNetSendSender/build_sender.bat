@echo OFF

echo Starting build...
if not exist .\out\ goto build

:buildexists
set /p ui="Existing build found. Overwrite? (y/n) "
if "%ui%" == "y" (
    if exist .\out\ rd /S /Q out\
    goto build
) 

if "%ui%" == "n" goto end

echo Invalid option.
goto buildexists

:build
if not exist bin\ mkdir bin\

javac --module-path "%JAVA_HOME%\jmods" .\src\*.java .\src\netsend\sender\*.java -d .\bin\ || goto end
jlink --module-path "%JAVA_HOME%\jmods";bin --add-modules jnetsend.sender --output out --launcher jnetsend=jnetsend.sender/netsend.sender.Sender || goto end

if exist .\out\bin\jnetsend del .\out\bin\jnetsend

echo Build successful.

:end
pause
