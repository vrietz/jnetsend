/*
 * JNetSend
 * Copyright (C) 2021  Vincent Rietz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package netsend.sender;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.StringJoiner;


/**
 * Sender part of JNetSend. 
 * Checks a list of ports and sends the given message to the first available.
 * 
 * @author Vincent Rietz
 */
public final class Sender {
    
    private static final Scanner IN = new Scanner(System.in);
    
    
    private Sender() {
        throw new UnsupportedOperationException();
    }
    
    
    /**
     * Main method. Formats message, opens socket and sends message via socket stream.
     * 
     * @param args
     *      Host and message to send, given by user
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("Error! No host given!");
            System.out.println("Usage: jnetsend [host] [message]");
            return;
        }
        
        String host = args[0];
        
        StringJoiner messageJoiner = new StringJoiner(" ");
        for (int i = 1; i < args.length; i++) {
            messageJoiner.add(args[i]);
        }
        
        if (messageJoiner.length() > 280) {
            System.err.println("Error! Message exceeds character limit [280] by " 
                    + (messageJoiner.length() - 280) + "characters!");
            return;
        }
        
        PrintWriter writer = null;
        System.out.println("Connecting...");
        
        try (Socket sender = getFreePortSocket(host, true)) {
            if (sender == null) {
                System.err.println("Error! Could not connect to host " + host + " !");
            } else {
                writer = new PrintWriter(sender.getOutputStream(), true);
                String message = messageJoiner.toString().trim();

                if (message.isEmpty()) {
                    if (userConfirmation("Send empty message to host " + host + " ?")) {
                        writer.println(message);
                        System.out.println("Message sent.");
                    }
                } else {
                    writer.println(message);
                    System.out.println("Message sent.");
                }
            }
        } catch (UnknownHostException e) {
            System.err.println("Error! Invalid IP address or host name: " + host + " !");
        } catch (IOException e) {
            System.err.println("Error! Failed to communicate with host: " + host + " !");
        }
        
        IN.close();
    }
    
    
    /*
     * Returns a Socket for the first open and available port from a list.
     */
    private static Socket getFreePortSocket(String host, boolean useTimeout) throws UnknownHostException {
        final int[] PORTLIST = {
            49164, 49188, 49236, 
            50448, 50472, 50520, 
            52932, 52956, 53004, 
            57816, 57840, 57888};
        
        Socket socket = null;
        int portIndex = 0;
        
        while (socket == null && portIndex < PORTLIST.length) {
            try {
                if (useTimeout) {
                    socket = new Socket();
                    socket.connect(new InetSocketAddress(
                            InetAddress.getByName(host), PORTLIST[portIndex]), 200);
                } else {
                    socket = new Socket(host, PORTLIST[portIndex]);
                }
            } catch (SocketTimeoutException e) {
                if (portIndex == (PORTLIST.length - 1) 
                        && userConfirmation("It might take longer to connect to " + host + ". Try anyway?")) {
                    socket = getFreePortSocket(host, false);
                }
            } catch (UnknownHostException e) {
                throw e; //Rethrow exception for error handling
            } catch (IOException e) {
            } finally {
                portIndex++;
                
                if (socket != null && !socket.isConnected()) {
                    socket = null;
                }
            }
        }
        
        return socket;
    }
    
    /*
     * Requests the users confirmation of a specified message and returns the answer.
     */
    private static boolean userConfirmation(String message) {
        String userinput = "";
        boolean inputInvalid = true;
        boolean userChoice = false;

        do {
            System.out.print(message + " (y/n) ");
            userinput = IN.nextLine();

            if (userinput.equals("y")) {
                inputInvalid = false;
                userChoice = true;
            } else if (userinput.equals("n")) {
                inputInvalid = false;
            } else {
                System.out.println("Invalid choice! Try again.");
            }
        } while (inputInvalid);

        return userChoice;
    }
}
