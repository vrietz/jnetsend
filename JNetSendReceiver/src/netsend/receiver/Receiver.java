/*
 * JNetSend
 * Copyright (C) 2021  Vincent Rietz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package netsend.receiver;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;


/**
 * Receiver part of JNetSend. Receives a message by listening to one of several ports 
 * and shows it using a simple dialog. Displays a tray icon while listening (or a small
 * window for some operating systems).
 * 
 * @author Vincent Rietz
 */
public final class Receiver {
    
    private static final String APP_NAME = "JNetSend";
    private static final ImageIcon ICON = new ImageIcon(Receiver.class.getResource("icon.png"));
    

    /*
     * Initializes and shows the tray icon or the alternative status window.
     */
    private Receiver() throws IllegalStateException {
        if (SystemTray.isSupported()) {
            SystemTray systray = SystemTray.getSystemTray();

            TrayIcon trayIcon = new TrayIcon(ICON.getImage());
            trayIcon.setImageAutoSize(true);
            trayIcon.setToolTip(APP_NAME);

            PopupMenu popup = new PopupMenu();
            MenuItem exitItem = new MenuItem("Exit");
            popup.add(exitItem);

            trayIcon.setPopupMenu(popup);

            try {
                systray.add(trayIcon);
            } catch (AWTException e) {
                throw new IllegalStateException("Could not initialize tray application!");
            }

            exitItem.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    systray.remove(trayIcon);
                    System.exit(0);
                }
            });
        } else {
            JFrame statusFrame = new JFrame(APP_NAME);
            
            JLabel text = new JLabel(" JNetSendReceiver is waiting for messages... ");
            text.setFont(UIManager.getLookAndFeelDefaults().getFont("OptionPane.messageFont"));
            
            JButton exitButton = new JButton("Exit");
            exitButton.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
            });
            
            statusFrame.setIconImage(ICON.getImage());
            statusFrame.setLayout(new BorderLayout());
            
            statusFrame.add(text, BorderLayout.CENTER);
            statusFrame.add(exitButton, BorderLayout.SOUTH);
            
            statusFrame.setLocationRelativeTo(null);
            statusFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            statusFrame.pack();
            statusFrame.setResizable(false);
            statusFrame.setVisible(true);
        }
    }

    /*
     * Starts a swing worker for message display.
     */
    private void start() {
        SwingWorker<Void, Message> worker = new SwingWorker<Void, Message>() {

            @Override
            protected Void doInBackground() {
                BufferedReader reader = null;
                Socket sender = null;

                try (ServerSocket receiver = getFreePortServerSocket()) {
                    if (receiver == null) {
                        throw new IllegalStateException("No free port available!");
                    } else {
                        while (!isCancelled()) {
                            sender = receiver.accept(); 

                            try {
                                reader = new BufferedReader(
                                        new InputStreamReader(sender.getInputStream()));
                                String message = reader.readLine();
                                if (message != null) {
                                    publish(new Message(sender, LocalDateTime.now(), message));
                                }
                            } catch (IOException e) {
                                // Ignore exception to keep receiver running if client fails
                            }
                        }
                    }
                } catch (IOException e) {
                    throw new IllegalStateException("Receiver communication failed!");
                }

                return null;
            }

            @Override
            protected void process(List<Message> messageList) {
                for (Message message : messageList) {
                    showMessage(message);
                }
            }

            @Override
            protected void done() {
                try {
                    get();
                } catch (ExecutionException | InterruptedException e) {
                    showErrorAndExit(e.getCause().getMessage());
                }
            }
        };

        worker.execute();
    }

    /*
     * Display a message dialog.
     */
    private void showMessage(Message message) {
        JFrame dummyFrame = new JFrame();
        dummyFrame.setAlwaysOnTop(true);

        JTextArea textarea = new JTextArea(message.message, 5, 35);
        textarea.setFont(UIManager.getLookAndFeelDefaults().getFont("OptionPane.messageFont"));

        textarea.setLineWrap(true);
        textarea.setWrapStyleWord(true);
        textarea.setEditable(false);
        textarea.setBackground(null);
        textarea.setBorder(null);


        String title = "Message from " 
                + message.sender.getInetAddress().getHostName()
                + " (" 
                + message.dateTime.format(
                        DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)) + ")";

        JOptionPane.showMessageDialog(
                dummyFrame, 
                textarea, 
                title, 
                JOptionPane.INFORMATION_MESSAGE);
    }

    /*
     * Shows an error message an exits the program.
     */
    private static void showErrorAndExit(String errorMessage) {
        JFrame dummyFrame = new JFrame();
        dummyFrame.setAlwaysOnTop(true);

        JOptionPane.showMessageDialog(
                dummyFrame, 
                errorMessage, 
                APP_NAME + " - Error!", 
                JOptionPane.ERROR_MESSAGE);

        System.exit(1);
    }

    /*
     * Returns a ServerSocket for the first available port from a list.
     */
    private static ServerSocket getFreePortServerSocket() {
        final int[] portList = {
                49164, 49188, 49236, 
                50448, 50472, 50520, 
                52932, 52956, 53004, 
                57816, 57840, 57888};

        for (int port : portList) {
            try {
                return new ServerSocket(port);
            } catch (IOException e) {
                continue;
            }
        }

        return null;
    }

    /**
     * Main method. Starts listening for messages and displays them on reception. 
     * 
     * @param args
     *      ignored
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException 
                | InstantiationException 
                | IllegalAccessException 
                | UnsupportedLookAndFeelException e) {

            //Do nothing and fall back to default
        }

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    new Receiver().start();
                } catch (IllegalStateException e) {
                    showErrorAndExit(e.getMessage());
                }
            }
        });
    }

    /*
     * Class holding simple message properties like date and sender data.
     */
    private final class Message {

        private final Socket sender;
        private final LocalDateTime dateTime;
        private final String message;

        private Message(Socket sender, LocalDateTime dateTime, String message) {
            this.sender = sender;
            this.dateTime = dateTime;
            this.message = message;
        }
    }
}
