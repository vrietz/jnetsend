@echo OFF

echo Starting build...
if not exist .\out\ goto build

:buildexists
set /p ui="Existing build found. Overwrite? (y/n) "
if "%ui%" == "y" (
    if exist .\out\ rd /S /Q out\
    goto build
) 

if "%ui%" == "n" goto end

echo Invalid option.
goto buildexists

:build
if not exist bin\ mkdir bin\

javac --module-path "%JAVA_HOME%\jmods" .\src\*.java .\src\netsend\receiver\*.java -d .\bin\ || goto end
jlink --module-path "%JAVA_HOME%\jmods";bin --add-modules jnetsend.receiver --output out --launcher run=jnetsend.receiver/netsend.receiver.Receiver || goto end

if exist .\out\bin\run.bat (
    cd out\bin\
    findstr /V Receiver .\run.bat > .\receiver.bat
    echo start "" "%%DIR%%\javaw" %%JLINK_VM_OPTIONS%% -m jnetsend.receiver/netsend.receiver.Receiver %%* >> .\receiver.bat
    echo exit /b 0 >> .\receiver.bat

    del .\run.bat
    del .\run
)

echo Build successful.

:end
pause
