# Issues with linux build

**Currently it is not possible to build on systems other than Windows due to a known OpenJDK issue (jlink). See #11 for details.**

---

# JNetSend
Java implementation of something similar to the former Windows messenger service (`net send`).

## Requirements
Running the program requires no extra software. To use build scripts or build from source you need an *Java 11+ Development Kit* with a configured `JAVA_HOME` environment variable.

## Usage
### Sender (to send messages)
Unzip `jnetsend_sender.zip` from `release_win/` directory (for Windows) or build the program from source (see below), then run `out\bin\jnetsend.bat` from command line.  
`[host]` may be any visible host name or IP address from your local network (including `localhost`).  

**Syntax**:  
```
jnetsend.bat [host] [message]
```

### Receiver (to be able to receive messages)

Unzip `jnetsend_receiver.zip` from `release_win/` directory (for Windows) or build the program from source (for other platforms, see below), then run `out\bin\receiver.bat`. A tray icon should appear while the program is running in background waiting for messages.

## Build release

If you want to build the release version including all required files yourself, download the project, adapt the build scripts (`build_sender.bat / build_receiver.bat`) if necessary and run them. The generated files will be stored in the `out/` directory.

## License

The project is published under the GNU General Public License Version 3.
